#!/bin/sh

XML=$(curl -s "http://ip-api.com/xml?fields=lat,lon")
lat=$(echo $XML | xmllint --xpath "/query/lat/text()" --nocdata -)
long=$(echo $XML | xmllint --xpath "/query/lon/text()" --nocdata -)

xflux -l $lat -g $long -k 2000

